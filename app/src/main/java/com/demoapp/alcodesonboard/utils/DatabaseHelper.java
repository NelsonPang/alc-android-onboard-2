package com.demoapp.alcodesonboard.utils;

import android.content.Context;

import com.demoapp.alcodesonboard.database.DbOpenHelper;
import com.demoapp.alcodesonboard.database.entities.DaoMaster;
import com.demoapp.alcodesonboard.database.entities.DaoSession;

public class DatabaseHelper {

    private static DaoSession mInstance;

    public static DaoSession getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DatabaseHelper.class) {
                if (mInstance == null) {
                    DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "app");

                    mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
                }
            }
        }

        return mInstance;
    }

    private DatabaseHelper() {
    }
}
