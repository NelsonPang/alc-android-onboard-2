package com.demoapp.alcodesonboard.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demoapp.alcodesonboard.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;
    private static Activity mActivity;

    public FriendListAdapter(Activity activity) {
        mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String firstName;
        public String lastName;
        public String email;
        public String avatar;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.textview_first_name_friend_list)
        public TextView textViewFirstNameFriendList;

        @BindView(R.id.textview_last_name_friend_list)
        public TextView textViewLastNameFriendList;

        @BindView(R.id.textview_email_friend_list)
        public TextView textViewEmailFriendList;

        @BindView(R.id.imageview_avatar_friend_list)
        protected ImageView mImageViewAvatarFriendList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_launcher_foreground);

                Glide.with(mActivity)
                        .load(data.avatar)
                        .apply(requestOptions)
                        .into(mImageViewAvatarFriendList);

                textViewFirstNameFriendList.setText(data.firstName);
                textViewLastNameFriendList.setText(data.lastName);
                textViewEmailFriendList.setText(data.email);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }

        public void resetViews() {
            textViewFirstNameFriendList.setText("");
            textViewLastNameFriendList.setText("");
            textViewEmailFriendList.setText("");
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);
    }
}
