package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.FriendListAdapter;
import com.demoapp.alcodesonboard.database.entities.FriendList;
import com.demoapp.alcodesonboard.database.entities.FriendListDao;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class FriendListRepository {

    private static FriendListRepository mInstance;

    private MutableLiveData<List<FriendListAdapter.DataHolder>> mFriendListAdapterListLiveData = new MutableLiveData<>();

    public static FriendListRepository getInstance() {
        if (mInstance == null) {
            synchronized (FriendListRepository.class) {
                mInstance = new FriendListRepository();
            }
        }

        return mInstance;
    }

    private FriendListRepository() {
    }

    public LiveData<List<FriendListAdapter.DataHolder>> getFriendListAdapterListLiveData() {
        return mFriendListAdapterListLiveData;
    }

    public void loadFriendListAdapterList(Context context) {
        List<FriendListAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<FriendList> records = DatabaseHelper.getInstance(context)
                .getFriendListDao()
                .loadAll();

        if (records != null) {
            for (FriendList friendList : records) {
                FriendListAdapter.DataHolder dataHolder = new FriendListAdapter.DataHolder();
                dataHolder.id = friendList.getId();
                dataHolder.firstName = friendList.getFirstName();
                dataHolder.lastName = friendList.getLastName();
                dataHolder.email = friendList.getEmail();
                dataHolder.avatar = friendList.getAvatar();


                dataHolders.add(dataHolder);
            }
        }

        mFriendListAdapterListLiveData.setValue(dataHolders);
    }

    public void addFriend(Context context, String firstName, String lastName, String email, String avatar) {
        // Create new record.
        FriendList friendList = new FriendList();
        friendList.setFirstName(firstName);
        friendList.setLastName(lastName);
        friendList.setEmail(email);
        friendList.setAvatar(avatar);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getFriendListDao()
                .insert(friendList);

        // Done adding record, now re-load list.
        loadFriendListAdapterList(context);
    }

    public void editFriend(Context context, Long id, String firstName, String lastName, String email) {
        FriendListDao friendListDao = DatabaseHelper.getInstance(context).getFriendListDao();
        FriendList friendList = friendListDao.load(id);

        // Check if record exists.
        if (friendList != null) {
            // Record is found, now update.
            friendList.setFirstName(firstName);
            friendList.setLastName(lastName);
            friendList.setEmail(email);

            friendListDao.update(friendList);

            // Done editing record, now re-load list.
            loadFriendListAdapterList(context);
        }
    }

    public void deleteFriend(Context context, Long id) {
        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getFriendListDao()
                .deleteByKey(id);

        // Done deleting record, now re-load list.
        loadFriendListAdapterList(context);
    }
}
