package com.demoapp.alcodesonboard.viewmodels.FriendListViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class FriendListViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public FriendListViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FriendListViewModel(mApplication);
    }
}
