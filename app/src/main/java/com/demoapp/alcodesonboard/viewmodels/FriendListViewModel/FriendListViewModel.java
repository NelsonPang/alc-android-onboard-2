package com.demoapp.alcodesonboard.viewmodels.FriendListViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.FriendListAdapter;
import com.demoapp.alcodesonboard.repositories.FriendListRepository;

import java.util.List;

public class FriendListViewModel extends AndroidViewModel {

    private FriendListRepository mFriendListRepository;

    public FriendListViewModel(@NonNull Application application) {
        super(application);

        mFriendListRepository = FriendListRepository.getInstance();
    }

    public LiveData<List<FriendListAdapter.DataHolder>> getFriendListAdapterListLiveData() {
        return mFriendListRepository.getFriendListAdapterListLiveData();
    }

    public void loadFriendListAdapterList() {
        mFriendListRepository.loadFriendListAdapterList(getApplication());
    }

    public void addFriend(String firstName, String lastName, String email, String avatar) {
        mFriendListRepository.addFriend(getApplication(), firstName, lastName, email, avatar);
    }

    public void editFriend(Long id, String firstName, String lastName, String email) {
        mFriendListRepository.editFriend(getApplication(), id, firstName, lastName, email);
    }

    public void deleteFriend(Long id) {
        mFriendListRepository.deleteFriend(getApplication(), id);
    }
}


