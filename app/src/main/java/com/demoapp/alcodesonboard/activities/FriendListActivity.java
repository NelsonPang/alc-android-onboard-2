package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.FriendListFragment;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;

import butterknife.ButterKnife;

public class FriendListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friend_list);

        ButterKnife.bind(this);

        // Check user is logged in.
        if (!SharedPreferenceHelper.getInstance(this).contains("token")) {
            // User is not login yet.
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {

            FragmentManager fragmentManager = getSupportFragmentManager();

            if (fragmentManager.findFragmentByTag(FriendListFragment.TAG) == null) {
                // Init fragment.
                fragmentManager.beginTransaction()
                        .replace(R.id.framelayout_fragment_holder, FriendListFragment.newInstance(), FriendListFragment.TAG)
                        .commit();
            }
        }
    }
}
