package com.demoapp.alcodesonboard.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.demoapp.alcodesonboard.database.entities.DaoMaster;

public class DbOpenHelper extends DaoMaster.OpenHelper {

    private Context mContext;

    public DbOpenHelper(Context context, String name) {
        super(context, name);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);

        //TODO Reminder for myself to initial data into database when database is created
//        db.execSQL("INSERT INTO " + FriendListDao.TABLENAME + " (" +
//                FriendListDao.Properties.Id.columnName + ", " +
//                FriendListDao.Properties.FirstName.columnName + ", " +
//                FriendListDao.Properties.LastName.columnName + ", " +
//                FriendListDao.Properties.Email.columnName + ", " +
//                FriendListDao.Properties.Avatar.columnName +
//                ") VALUES(1, 'Nelson', 'Pang Keit Hoe', 'nelsonpangkeithoe@gmail.com', 'AvatarURL')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }
}
