package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.FriendDetailActivity;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.adapters.FriendListAdapter;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.FriendListViewModel.FriendListViewModel;
import com.demoapp.alcodesonboard.viewmodels.FriendListViewModel.FriendListViewModelFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FriendListFragment extends Fragment implements FriendListAdapter.Callbacks {

    public static final String TAG = FriendListFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;

//    @BindView(R.id.textview_no_friend_list)
//    protected TextView mTextViewNoFriendList;

    @BindView(R.id.textview_have_friend_list)
    protected TextView mTextViewHaveFriendList;

    @BindView(R.id.button_load_data_from_api)
    protected Button mButtonLoadDataFromApi;

    private final int REQUEST_CODE_FRIEND_DETAIL = 300;

    private Unbinder mUnbinder;
    private FriendListAdapter mAdapter;
    private FriendListViewModel mViewModel;

    public FriendListFragment() {
    }

    public static FriendListFragment newInstance() {
        return new FriendListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_friend_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_logout) {
            new MaterialDialog.Builder(getActivity())
                    .title("Logout")
                    .content("Are you sure you want to logout?")
                    .positiveText("Yes")
                    .negativeText("No")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // Logout user.
                            // Clear all user's data.
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();

                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();

                            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                                Toast.makeText(getActivity().getApplicationContext(), "Logged out!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        }
                    })
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_FRIEND_DETAIL && resultCode == FriendDetailActivity.RESULT_LASTNAME_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadFriendListAdapterList();
        }
    }

    @Override
    public void onListItemClicked(FriendListAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), FriendDetailActivity.class);
        intent.putExtra(FriendDetailActivity.EXTRA_LONG_FRIEND_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_FRIEND_DETAIL);
    }

    @Override
    public void onDeleteButtonClicked(FriendListAdapter.DataHolder data) {
        mViewModel.deleteFriend(data.id);
    }

    private void initView() {
        mAdapter = new FriendListAdapter(getActivity());
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new FriendListViewModelFactory(getActivity().getApplication())).get(FriendListViewModel.class);
        mViewModel.getFriendListAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<FriendListAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<FriendListAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                int num = mAdapter.getItemCount();

                if (num < 1) {
                    mTextViewHaveFriendList.setText(num + " friend");
                } else {
                    mTextViewHaveFriendList.setText(num + " friends");
                }

                if (dataHolders.isEmpty()) {
//                    mTextViewNoFriendList.setVisibility(View.VISIBLE);
                    mButtonLoadDataFromApi.setVisibility(View.VISIBLE);
                } else {
//                    mTextViewNoFriendList.setVisibility(View.GONE);
                    mButtonLoadDataFromApi.setVisibility(View.GONE);
                }
            }
        });

        // Load data into adapter.
        mViewModel.loadFriendListAdapterList();
    }

    @OnClick(R.id.button_load_data_from_api)
    public void OnClicked() {
        String url = "https://reqres.in/api/users?page=2";

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String firstName = jsonObject.getString("first_name");
                        String lastName = jsonObject.getString("last_name");
                        String email = jsonObject.getString("email");
                        String avatar = jsonObject.getString("avatar");

                        mViewModel = new ViewModelProvider(getActivity(), new FriendListViewModelFactory(getActivity().getApplication())).get(FriendListViewModel.class);

                        mViewModel.addFriend(firstName, lastName, email, avatar);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

}
