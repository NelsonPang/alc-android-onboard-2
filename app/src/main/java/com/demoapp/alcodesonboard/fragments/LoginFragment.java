package com.demoapp.alcodesonboard.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.FriendListActivity;
import com.demoapp.alcodesonboard.activities.TermsOfUseActivity;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_password)
    protected TextInputEditText mEditTextPasword;

    @BindView(R.id.button_login)
    protected MaterialButton mButtonLogin;

    private Unbinder mUnbinder;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPasword.setText("cityslicka");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick({R.id.terms_of_use_textview})
    protected void doTermsOfUse() {
        Intent intent = new Intent(getContext(), TermsOfUseActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button_login)
    protected void doLogin() {
        // Disable button and wait for server response.
        mButtonLogin.setEnabled(false);

        String original_label = getString(R.string.original_label);
        String on_click_label = getString(R.string.on_click_label);
        final String CHANGE_BACK_LABEL = original_label;

        original_label = on_click_label;

        mButtonLogin.setText(original_label);

        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPasword.getText().toString(); // Password should not trim.

        if (email.isEmpty() || password.isEmpty()) {
            new MaterialDialog.Builder(getActivity())
                    .title("Empty Email or Password")
                    .content("Email or password should not be empty!")
                    .positiveText("OK")
                    .show();
            mButtonLogin.setText(CHANGE_BACK_LABEL);
            mButtonLogin.setEnabled(true);
            return;
        }

        // Call login API.
        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                // Convert JSON string to Java object.
                LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                // Save user's email to shared preference.
                SharedPreferenceHelper.getInstance(getActivity())
                        .edit()
                        .putString("email", email)
                        .putString("token", responseModel.token)
                        .apply();

                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                    String loggedIn = SharedPreferenceHelper.getInstance(getActivity()).getString("email", "");

                    if (loggedIn != null || loggedIn != "") {
                        Toast.makeText(getActivity().getApplicationContext(), "Logged in!", Toast.LENGTH_SHORT).show();
                    }
                }

                startActivity(new Intent(getActivity(), FriendListActivity.class));
                getActivity().finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
                    new MaterialDialog.Builder(getActivity())
                            .title("No Internet")
                            .content("Internet is not available, please check your internet connection!")
                            .positiveText("OK")
                            .show();
                } else {

                    Timber.e("d;;Login error: %s", error.getMessage());

                    if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                        // Show error in popup dialog.
                        new MaterialDialog.Builder(getActivity())
                                .title("Wrong Email or Password")
                                .content("Invalid email or password!")
                                .positiveText("OK")
                                .show();
                    }
                }

                mButtonLogin.setText(CHANGE_BACK_LABEL);

                mButtonLogin.setEnabled(true);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }
}
