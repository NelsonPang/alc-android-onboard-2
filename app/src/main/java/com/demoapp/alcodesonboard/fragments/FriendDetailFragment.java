package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.database.entities.FriendList;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.viewmodels.FriendListViewModel.FriendListViewModel;
import com.demoapp.alcodesonboard.viewmodels.FriendListViewModel.FriendListViewModelFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendDetailFragment extends Fragment {

    public static final String TAG = FriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_FRIEND_ID = "ARG_LONG_FRIEND_ID";

    @BindView(R.id.textview_first_name_friend_detail)
    protected TextView mTextViewFirstNameFriendDetail;

    @BindView(R.id.textview_last_name_friend_detail)
    protected TextView mTextViewLastNameFriendDetail;

    @BindView(R.id.textview_email_friend_detail)
    protected TextView mTextViewEmailFriendDetail;

    @BindView(R.id.imageview_avatar_friend_detail)
    protected ImageView mImageViewAvatar;

    private Unbinder mUnbinder;
    private Long mFriendId = 0L;
    private FriendListViewModel mViewModel;

    public FriendDetailFragment() {
    }

    public static FriendDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_FRIEND_ID, id);

        FriendDetailFragment fragment = new FriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mFriendId = args.getLong(ARG_LONG_FRIEND_ID, 0);
        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    private void initView() {

        if (mFriendId > 0) {
            FriendList friendList = DatabaseHelper.getInstance(getActivity())
                    .getFriendListDao()
                    .load(mFriendId);

            if (friendList != null) {

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_launcher_foreground);

                Glide.with(getContext())
                        .load(friendList.getAvatar())
                        .apply(requestOptions)
                        .into(mImageViewAvatar);

                mTextViewFirstNameFriendDetail.setText(friendList.getFirstName());
                mTextViewLastNameFriendDetail.setText(friendList.getLastName());
                mTextViewEmailFriendDetail.setText(friendList.getEmail());
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Friend not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new FriendListViewModelFactory(getActivity().getApplication())).get(FriendListViewModel.class);

        // Load data into adapter.
        mViewModel.loadFriendListAdapterList();
    }
}
