package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demoapp.alcodesonboard.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TermsOfUseFragment extends Fragment {
    public static final String TAG = TermsOfUseFragment.class.getSimpleName();

    private Unbinder mUnbinder;

    public TermsOfUseFragment() {
    }

    public static TermsOfUseFragment newInstance() {
        return new TermsOfUseFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_of_use, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO Reminder for myself to store HTML data into variable and show it in webview
//        String data = "<h2 style=\"color:white\">General Terms</h3> <h3 style=\"color:white\">By accessing and placing an order with ABC, you confirm that " +
//                "you are in agreement with and bound by the terms of service outlined below. These terms apply to the " +
//                "entire mobile application.</h3> <h3 style=\"color:white\">Under no circumstances shall ABC team be liable for any " +
//                "direct, indirect, special, incidental or consequential damages, including, but not limited to, loss " +
//                "of data or profit, arising out of the use, or the inability to use, the materials on this mobile " +
//                "application, even if team or an authorized representative has been advised of the possibility of " +
//                "such damages.</h3>";

        WebView webview = (WebView) this.getActivity().findViewById(R.id.webview_terms_of_use);
        webview.getSettings().setJavaScriptEnabled(true);
//        webview.loadData(data, "text/html; charset=utf-8", "UTF-8");
        webview.loadUrl("https://usabilla.com/terms/");
//        webview.setBackgroundColor(Color.RED);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

}
